import discord
import os
from discord.ext.commands import Bot
from discord import Intents
from dotenv import load_dotenv
load_dotenv()
# intents = Intents.all()

# client = bot(intents=intents, command_prefix='$')
client = Bot(command_prefix='$')

@client.event
async def on_ready():
	print(f'Logged in as {client.user}')
	await client.change_presence(activity = discord.Activity(
		type = discord.ActivityType.watching,
		name = 'Nothing but the skies'
	))

@client.command(name='hello')
async def hewwo(ctx):
	await ctx.send('Hewwo!')

@client.command(name='embed')
async def embeds(ctx):
	embed=discord.Embed(title=f'Hello! {ctx.author}', description='As this bot is still in development most of the commands aren\'t available\n\nHave a good day!')
	await ctx.send(embed=embed)

client.run(os.getenv('TOKEN'))